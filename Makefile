IDIR= ./includes
CC=gcc
CFLAGS=-c -I$(IDIR) -lm
DEPS = point.h

all: DistanciaEuclidiana

DistanciaEuclidiana: main.o point.o ./includes/point.h
	$(CC) -o DistanciaEuclidiana main.o point.o -I$(IDIR) -lm

main.o: main.c
	$(CC) $(CFLAGS) main.c

point.o: point.c
	$(CC) $(CFLAGS) point.c

clean:
	rm *o DistanciaEuclidiana

