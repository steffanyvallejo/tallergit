#include <point.h>
#include <math.h>
#include <stdio.h>

float calcularDistancia(Point p1,Point p2){
	float r1,r2,r3,suma;
	r1 = pow(p2.x - p1.x,2);
	r2 = pow(p2.y - p1.y,2);
	r3 = pow(p2.z - p1.z,2);
	suma=r1+r2+r3;
return sqrt(suma);
}

Point puntoMedio(Point p1, Point p2){
	Point punto_medio;
	punto_medio.x = ((p1.x + p2.x)/2);
	punto_medio.y = ((p1.y + p2.y)/2);
	punto_medio.z = ((p1.z + p2.z)/2);
return punto_medio;
}
